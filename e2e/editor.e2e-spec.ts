import { EditorPage } from './editor.po';

describe('Editor page', () => {
  let page: EditorPage;

  beforeEach(() => {
    page = new EditorPage();
  });

  it('should add a new text field after clicking on the button', async () => {
    await page.navigateTo();
    const textFieldsBefore = await page.getAllTextFields().count();
    await page.getAddTextFieldButton().click();
    expect(await page.getAllTextFields().count()).toEqual(textFieldsBefore + 1);
  });
});
