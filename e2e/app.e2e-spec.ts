import { MainPage } from './app.po';
import { browser } from 'protractor';

describe('Main page', () => {
  let page: MainPage;

  beforeEach(() => {
    page = new MainPage();
  });

  it('should display a list of memes', async () => {
    await page.navigateTo();
    expect(await page.getAllMemes().count()).toBeGreaterThan(0);
  });

  it('should enable user to get to the editor', async () => {
    await page.navigateTo();
    await page.getMemeButton().click();
    expect(await browser.getCurrentUrl()).toContain('/editor');
  });
});
