import { browser, by, element } from 'protractor';

export class MainPage {
  navigateTo() {
    return browser.get('/');
  }

  getAllMemes() {
    return element.all(by.className('meme'));
  }

  getMemeButton() {
    return element(by.buttonText('Make a meme!'));
  }
}
