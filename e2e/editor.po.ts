import { browser, by, element } from 'protractor';

export class EditorPage {
  navigateTo() {
    return browser.get('editor');
  }

  getAddTextFieldButton() {
    return element(by.buttonText('Add text field'));
  }

  getAllTextFields() {
    return element.all(by.className('textField'));
  }
}
