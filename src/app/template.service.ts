import { throwError as observableThrowError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import {Template} from './template';

const API_URL = 'https://5yy6lo10z7.execute-api.us-east-1.amazonaws.com/Test';

@Injectable()
export class TemplateService {

  constructor(private http: HttpClient) { }

  public getTemplates(): Observable<Template[]> {
    return this.http
      .get<Template[]>(`${API_URL}/template`).pipe(
        map(response => response['Items']),
        catchError((error: Response | any) => {
          console.error('API Service call failed: ', error);
          return observableThrowError(error);
        }));
  }

}
