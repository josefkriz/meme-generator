import { Component, OnInit } from '@angular/core';
import { TextField } from '../textField';
import { MemeService } from '../meme.service';

declare var UIkit: any;

const STORAGE_URL = 'https://memegenerator.josefkriz.com/';

@Component({
  selector: 'app-meme-editor',
  templateUrl: './meme-editor.component.html',
  styleUrls: ['./meme-editor.component.scss']
})
export class MemeEditorComponent implements OnInit {

  canvas: HTMLCanvasElement;
  context: CanvasRenderingContext2D;
  dragOn: boolean;
  dragPossible: boolean;

  textFields: TextField[];
  activeField: TextField;
  defaultField: TextField;

  selectedImage: string;
  selectedOffsetX: number;
  selectedOffsetY: number;
  image = new Image();

  exportedImage: string;
  savedImage: string;
  loading: boolean;
  touched: boolean;

  constructor(private memeService: MemeService) {}

  ngOnInit() {
    this.canvas = <HTMLCanvasElement> document.getElementById('meme-editor');
    this.context = this.canvas.getContext('2d');
    this.dragOn = false;
    this.dragPossible = false;
    this.loading = false;
    this.canvas.height = 500;

    this.loadImage(`${STORAGE_URL}templates/doge.jpg`);

    this.textFields = [new TextField(this.canvas.width / 2, this.canvas.height - 10)];
    this.defaultField = new TextField(this.canvas.width / 2, Math.floor(Math.random() * this.canvas.height));
  }

  clearEditor(): void {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  updateEditor(): void {
    this.touched = true;
    this.clearEditor();
    this.context.drawImage(this.image, 0, 0, this.canvas.width, this.canvas.height);
    this.drawText();
  }

  drawText(): void {
    this.context.textAlign = 'center';
    this.context.textBaseline = 'middle';
    this.context.lineJoin = 'round'; // prevent incorrect generation of text shadows

    this.textFields.slice().reverse().forEach((textField) => { // iterating in reversed order to keep order
      if (textField.y > this.canvas.height) { textField.y = this.canvas.height; } // if the field goes out of canvas modify it's position

      const text = textField.upperCase ? textField.text.toUpperCase() : textField.text; // don't change the actual value in TextField
      this.context.font = textField.fontSize + 'px ' + textField.font;

      if (textField.textShadow) {
        this.context.shadowBlur = 10;
        this.context.shadowOffsetX = 0;
        this.context.shadowOffsetY = 0;
        this.context.shadowColor = textField.shadowColor;
        this.context.lineWidth = textField.fontSize / 8;
        this.context.strokeText(text, textField.x, textField.y);
        this.context.shadowBlur = 0;
      } else { this.context.shadowColor = 'transparent'; }

      this.context.fillStyle = textField.fontColor;
      this.context.fillText(text, textField.x, textField.y);
    });
  }

  mouseMove(e): void {
    if (this.dragOn) {
      this.activeField.x = e.pageX - this.canvas.offsetLeft + this.selectedOffsetX;
      this.activeField.y = e.pageY - this.canvas.offsetTop + this.selectedOffsetY;
      this.updateEditor();
    } else { this.dragPossible = this.cursorAboveTextField(e.pageX, e.pageY); }
  }

  mouseDown(e): void {
    if (this.cursorAboveTextField(e.pageX, e.pageY)) {
      this.selectedOffsetX = this.activeField.x + this.canvas.offsetLeft - e.pageX;
      this.selectedOffsetY = this.activeField.y + this.canvas.offsetTop - e.pageY;
      this.dragOn = true;
    }
  }

  mouseUp(): void {
    this.dragOn = false;
  }

  cursorAboveTextField(x: number, y: number): boolean {
    this.activeField = this.textFields.find((textField) => {
      const text = textField.upperCase ?  textField.text.toUpperCase() : textField.text; // don't change the actual value in TextField
      return x < textField.x + this.context.measureText(text).width / 2 + this.canvas.offsetLeft
        && x > textField.x - this.context.measureText(text).width / 2 + this.canvas.offsetLeft
        && y < textField.y + textField.fontSize / 2 + this.canvas.offsetTop
        && y > textField.y - textField.fontSize / 2 + this.canvas.offsetTop; });

    return this.activeField !== undefined;
  }

  addTextField(): void  {
    const newField = new TextField(
      this.canvas.width / 2,
      Math.floor(Math.random() * this.canvas.height),
      this.defaultField.text,
      this.defaultField.font,
      this.defaultField.fontColor,
      this.defaultField.fontSize,
      this.defaultField.upperCase,
      this.defaultField.textShadow,
      this.defaultField.shadowColor
    );
    this.textFields.push(newField);
    this.updateEditor();
  }

  removeTextField(field: TextField): void {
    const index = this.textFields.indexOf(field);
    if (index > -1) { this.textFields.splice(index, 1); }

    this.updateEditor();
  }

  loadImage(newImage: string): void {
    this.selectedImage = newImage;
    this.image.crossOrigin = 'anonymous'; // otherwise the canvas becomes 'tainted'
    this.image.onload = () => {
      this.canvas.height = (this.canvas.width / this.image.width) * this.image.height;
      setTimeout(() => this.updateEditor(), 1);
    };
    this.image.src = this.selectedImage;
  }

  centerTextField(field: TextField): void {
    field.x = this.canvas.width / 2;

    this.updateEditor();
  }

  setDefaultTextField(field: TextField): void {
    this.defaultField.font = field.font;
    this.defaultField.fontColor = field.fontColor;
    this.defaultField.fontSize = field.fontSize;
    this.defaultField.upperCase = field.upperCase;
    this.defaultField.textShadow = field.textShadow;
    this.defaultField.shadowColor = field.shadowColor;

    this.applyDefaultField();
  }

  applyDefaultField(): void {
    this.textFields.forEach((textField) => {
      textField.font = this.defaultField.font;
      textField.fontColor = this.defaultField.fontColor;
      textField.fontSize = this.defaultField.fontSize;
      textField.upperCase = this.defaultField.upperCase;
      textField.textShadow = this.defaultField.textShadow;
      textField.shadowColor = this.defaultField.shadowColor;
    });

    this.updateEditor();
  }

  exportCanvas(): void {
    this.exportedImage = this.canvas.toDataURL('image/png');
  }

  saveCanvas(): void {
    this.loading = true;
    this.touched = false;
    this.exportCanvas();
    const base64image = this.exportedImage.replace(/^data:image\/png;base64,/, ''); // delete the prefix
    this.memeService.saveMeme(base64image).subscribe(
      (meme) => {
        this.loading = false;
        this.savedImage = `${STORAGE_URL}memes/${meme.id}`; },
      () => {
        this.loading = false;
        this.touched = true;
        this.savedImage = undefined;
        UIkit.notification(
          '<span uk-icon=\'icon: warning\'></span> Error. Please try again later.',
          {status: 'danger', pos: 'top-right'}
        );
      }

    );
  }

  copyToClipboard(): void {
    UIkit.notification(
      '<span uk-icon=\'icon: check\'></span> Copied to clipboard.',
      {status: 'success', pos: 'top-right'}
    );
  }

}
