import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemeEditorComponent } from './meme-editor.component';
import { MemeService } from '../meme.service';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '@ngx-share/core';
import { ClipboardModule } from 'ngx-clipboard';
import { ColorPickerModule } from 'ngx-color-picker';
import { MemeTemplateGalleryComponent } from '../meme-template-gallery/meme-template-gallery.component';
import { HttpClient } from '@angular/common/http';
import { TemplateService } from '../template.service';

declare var UIkit: any;

describe('Meme editor component', () => {
  let component: MemeEditorComponent;
  let fixture: ComponentFixture<MemeEditorComponent>;

  const mockMemeService: Partial<MemeService> = {
    saveMeme: () => of({id: 'test.png', created: 123456789, thumbsDown: 0, thumbsUp: 0})
  };

  const mockTemplateService: Partial<TemplateService> = {
    getTemplates: () => of([])
  };

  beforeEach(async(async () => {
    UIkit.notification = () => {};

    await TestBed.configureTestingModule({
      declarations: [ MemeEditorComponent, MemeTemplateGalleryComponent ],
      providers: [
        { provide: MemeService, useValue: mockMemeService },
        { provide: TemplateService, useValue: mockTemplateService },
        { provide: HttpClient, useValue: {} }
      ],
      imports: [ FormsModule,
        ColorPickerModule,
        ClipboardModule,
        ShareModule.forRoot()
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MemeEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    await fixture.whenStable();
  }));

  it('should add a text field after clicking on a button', async(async () => {
    const html = fixture.debugElement.nativeElement;
    const addTextFieldButton = html.querySelector('#addTextFieldBtn') as HTMLElement;
    const textFields = html.querySelectorAll('.textField');
    const numberOfTextFieldsBefore = textFields.length;

    addTextFieldButton.click();
    fixture.detectChanges();
    await fixture.whenStable();

    const newTextFields = html.querySelectorAll('.textField');

    expect(newTextFields.length).toEqual(numberOfTextFieldsBefore + 1);
  }));

});
