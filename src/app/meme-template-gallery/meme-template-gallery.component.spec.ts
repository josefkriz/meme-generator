import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MemeTemplateGalleryComponent } from './meme-template-gallery.component';
import { TemplateService } from '../template.service';
import { of } from 'rxjs';
import { Template } from '../template';
import { By } from '@angular/platform-browser';

declare var UIkit: any;

describe('Meme template component', () => {
  let component: MemeTemplateGalleryComponent;
  let fixture: ComponentFixture<MemeTemplateGalleryComponent>;

  let html: HTMLElement;

  const mockTemplates: Template[] = [
    {
      'id': 'distracted-boyfriend.jpg',
      'name': 'Distracted Boyfriend',
      'tags': ['distracted', 'boyfriend', 'man', 'looking', 'at', 'other', 'woman']
    }, {
      'id': 'doge.jpg',
      'name': 'Doge',
      'tags': ['doge', 'dog', 'shiba', 'inu']
    }, {
      'id': 'fry.jpg',
      'name': 'Futurama Fry (Not Sure If)',
      'tags': ['fry', 'futurama', 'not', 'sure', 'if']
    }];

  const mockTemplateService: Partial<TemplateService> = {
    getTemplates: () => of(mockTemplates)
  };

  beforeEach(async(async () => {
    spyOn(mockTemplateService, 'getTemplates').and.callThrough();
    UIkit.notification = () => {};

    await TestBed.configureTestingModule({
      declarations: [ MemeTemplateGalleryComponent ],
      providers: [
        {provide: TemplateService, useValue: mockTemplateService}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MemeTemplateGalleryComponent);
    component = fixture.componentInstance;
    html = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    await fixture.whenStable();

  }));

  it('should display list of templates', () => {
    const templates = html.querySelectorAll('.template-card');

    expect(templates.length).toBe(3);
  });

  it('should raise loadImage event after clicking on a template', (done) => {
    const testTemplate = fixture.debugElement.queryAll(By.css('.template-card'))[1];

    component.loadImage.subscribe((loadedTemplate: string) => {
      expect(loadedTemplate).toBe(mockTemplates[1].id);
      done();
    });

    testTemplate.triggerEventHandler('click', null);

  });
});
