import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TemplateService } from '../template.service';
import { Template } from '../template';
declare var UIkit: any;

const STORAGE_URL = 'https://memegenerator.josefkriz.com/';

@Component({
  selector: 'app-meme-templates',
  templateUrl: './meme-template-gallery.component.html',
  styleUrls: ['./meme-template-gallery.component.scss']
})
export class MemeTemplateGalleryComponent implements OnInit {
  @Input() selectedImage: string;
  @Output() loadImage = new EventEmitter<string>();

  templates: Template[];
  loading: boolean;

  constructor(private templateService: TemplateService) { }

  ngOnInit() {
    this.templates = [];
    this.getImages();
    this.loading = true;
  }

  getImages(): void {
    this.templateService.getTemplates().subscribe(
      (images) => {
        this.templates = images;
        this.templates.forEach((template) => template.id = `${STORAGE_URL}templates/${template.id}`); // use absolute urls instead of ids
        this.loading = false;
      },
      () => {
        this.loading = false;
        UIkit.notification(
          '<span uk-icon=\'icon: warning\'></span> Couldn\'t fetch meme templates. Please try again later.',
          {status: 'danger', pos: 'top-right'}
        );
      });
  }

  select(newSelected: Template): void {
    this.loadImage.emit(newSelected.id);
  }

  allowDrop(event): void {
    event.stopPropagation();
    event.preventDefault();
  }

  handleImage(event): void {
    event.stopPropagation();
    event.preventDefault();

    const target = event instanceof DragEvent ? event.dataTransfer : event.target;

    if (target.files[0]) { // if a file was uploaded
      if (this.isImage(target.files[0])) { // if the file is an image

        const reader = new FileReader();
        reader.onload = (loadEvent: any) => {
          this.templates.push(new Template(loadEvent.target.result, 'Your template', []));
          this.loadImage.emit(loadEvent.target.result);
        };
        reader.readAsDataURL(target.files[0]);
      } else {
        UIkit.notification(
          '<span uk-icon=\'icon: warning\'></span> Uploaded file is not an image.',
          {status: 'danger', pos: 'top-right'}
        );
      }
    }
  }

  isImage(file): boolean {
    return file.type.match('image.*');
  }
}
