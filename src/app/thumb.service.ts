import { throwError as observableThrowError, Observable } from 'rxjs';
import { catchError, mapTo } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meme } from './meme';

const API_URL = 'https://5yy6lo10z7.execute-api.us-east-1.amazonaws.com/Test';

@Injectable()
export class ThumbService {

  constructor(private http: HttpClient) { }

  public giveThumb(meme: Meme, kind: string): Observable<void> {
    return this.http
      .post(`${API_URL}/meme/thumb`, {id: meme.id, created: meme.created, kind: kind}).pipe(
        mapTo(null),
        catchError((error: Response | any) => {
          console.error('API Service call failed: ', error);
          return observableThrowError(error);
        }));
  }

}
