export class TextField {
  text: string;
  x: number;
  y: number;
  font: string;
  fontSize: number;
  fontColor: string;
  upperCase: boolean;
  textShadow: boolean;
  shadowColor: string;

  constructor(x: number, y: number, text = 'New text field', font = 'Impact',
              fontColor = '#ffffff', fontSize = 35, upperCase = true, textShadow = true,
              shadowColor = '#000000') {
    this.text = text;
    this.font = font;
    this.fontColor = fontColor;
    this.fontSize = fontSize;
    this.x = x;
    this.y = y;
    this.upperCase = upperCase;
    this.textShadow = textShadow;
    this.shadowColor = shadowColor;
  }
}
