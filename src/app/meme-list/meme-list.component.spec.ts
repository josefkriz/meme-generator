import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemeListComponent } from './meme-list.component';
import { MemeService } from '../meme.service';
import { of } from 'rxjs';
import { ThumbService } from '../thumb.service';

declare var UIkit: any;

describe('Meme list component', () => {
  let component: MemeListComponent;
  let fixture: ComponentFixture<MemeListComponent>;

  let html: HTMLElement;

  const mockMemeService: Partial<MemeService> = {
    getMemes: () => of([
      {id: 'test.png', created: 123456789, thumbsDown: 123, thumbsUp: 312},
      {id: 'test2.png', created: 323456789, thumbsDown: 0, thumbsUp: 1}
    ])
  };

  const mockThumbService: Partial<ThumbService> = {
    giveThumb: () => of(null)
  };

  beforeEach(async(async () => {
    spyOn(mockThumbService, 'giveThumb').and.callThrough();
    UIkit.notification = () => {};

    await TestBed.configureTestingModule({
      declarations: [ MemeListComponent ],
      providers: [
        {provide: MemeService, useValue: mockMemeService},
        {provide: ThumbService, useValue: mockThumbService}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MemeListComponent);
    component = fixture.componentInstance;
    html = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    await fixture.whenStable();

  }));

  it('should display list of memes', () => {
    const memes = html.querySelectorAll('.meme');

    expect(memes.length).toBe(2);
  });

  it('should send thumbs', async(async () => {
    const thumbs = html.querySelector('.thumb-up') as HTMLLinkElement;

    thumbs.click();
    await fixture.whenStable();

    expect(mockThumbService.giveThumb).toHaveBeenCalled();
  }));

  it('should prevent user from sending multiple thumbs to one meme', async(async () => {
    const thumbs = html.querySelector('.thumb-up') as HTMLLinkElement;

    thumbs.click();
    await fixture.whenStable();

    thumbs.click();
    await fixture.whenStable();

    thumbs.click();
    await fixture.whenStable();

    expect(mockThumbService.giveThumb).toHaveBeenCalledTimes(1);

  }));
});
