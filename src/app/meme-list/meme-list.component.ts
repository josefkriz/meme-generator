import { Component, OnInit } from '@angular/core';
import { MemeService } from '../meme.service';
import { Meme } from '../meme';
import { ThumbService } from '../thumb.service';
declare var UIkit: any;

@Component({
  selector: 'app-meme-list',
  templateUrl: './meme-list.component.html',
  styleUrls: ['./meme-list.component.scss']
})
export class MemeListComponent implements OnInit {

  clicked: string[]; // list of memes liked/disliked by a user
  memes: Meme[];
  now: number;
  loading: boolean;
  memesPerPage: number;
  currentPage: number;

  constructor(private memeService: MemeService, private thumbService: ThumbService) { }

  ngOnInit() {
    this.getMemes();
    this.now = Date.now();
    this.clicked = [];
    this.loading = true;
    this.memesPerPage = 20;
    this.currentPage = 0;
  }

  getMemes(): void {
    this.memeService.getMemes().subscribe(
      (memes) => {
        this.memes = memes.sort((a: Meme, b: Meme) => b.created - a.created);
        this.loading = false;
      },
      () => {
        this.loading = false;
        UIkit.notification(
          '<span uk-icon=\'icon: warning\'></span> Couldn\'t fetch memes. Please try again later.',
          {status: 'danger', pos: 'top-right'}
        );
      });
  }

  giveThumb(meme: Meme, kind: string): void {
    if (this.clicked.includes(meme.id)) {
        return UIkit.notification(
          '<span uk-icon=\'icon: info\'></span> You already rated this meme.',
          {status: 'warning', pos: 'top-right'}
        );
    }

    this.clicked.push(meme.id); // add to the list of already rated memes

    this.thumbService.giveThumb(meme, kind).subscribe(
      () => {
        UIkit.notification(
          '<span uk-icon=\'icon: check\'></span> Changes saved.',
          {status: 'success', pos: 'top-right'}
        );
        this.getMemes(); // refresh the list
      },
      () => {
        this.clicked.pop(); // remove from the list of already rated memes
        UIkit.notification(
          '<span uk-icon=\'icon: warning\'></span> Error. Please try again later.',
          {status: 'danger', pos: 'top-right'}
        );
      });
  }

  canGoToNextPage(): boolean {
    return this.memes && (this.currentPage + 1) * this.memesPerPage < this.memes.length;
  }

  canGoToPreviousPage(): boolean {
    return this.memes && this.currentPage > 0;
  }

  nextPage(): void {
    if (this.canGoToNextPage()) { this.currentPage++; }
  }

  previousPage(): void {
    if (this.canGoToPreviousPage()) { this.currentPage--; }
  }

}
