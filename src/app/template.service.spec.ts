import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { defer } from 'rxjs/index';

import {Template} from './template';
import {TemplateService} from './template.service';

let httpClientSpy: { get: jasmine.Spy };
let templateService: TemplateService;

describe ('Template service: get templates', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    templateService = new TemplateService(<any> httpClientSpy);
  });

  it('should return templates', (done) => {
    const mockResponse = {
      'Items': [
        {
          'id': 'distracted-boyfriend.jpg',
          'name': 'Distracted Boyfriend',
          'tags': ['distracted', 'boyfriend', 'man', 'looking', 'at', 'other', 'woman']
        },
        {
          'id': 'doge.jpg',
          'name': 'Doge',
          'tags': ['doge', 'dog', 'shiba', 'inu']
        },
        {
          'id': 'fry.jpg',
          'name': 'Futurama Fry (Not Sure If)',
          'tags': ['fry', 'futurama', 'not', 'sure', 'if']
        }],
      'Count': 3,
      'ScannedCount': 3
    };

    const expectedTemplates: Template[] = [
      {
        'id': 'distracted-boyfriend.jpg',
        'name': 'Distracted Boyfriend',
        'tags': ['distracted', 'boyfriend', 'man', 'looking', 'at', 'other', 'woman']
      },
      {
        'id': 'doge.jpg',
        'name': 'Doge',
        'tags': ['doge', 'dog', 'shiba', 'inu']
      },
      {
        'id': 'fry.jpg',
        'name': 'Futurama Fry (Not Sure If)',
        'tags': ['fry', 'futurama', 'not', 'sure', 'if']}
    ];

    httpClientSpy.get.and.returnValue(defer(() => Promise.resolve(mockResponse)));

    templateService.getTemplates().subscribe(
      templates => {
        expect(templates).toEqual(expectedTemplates, 'expected templates');
        expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
        done();
      },
      fail
    );
  });

  it('should return an error when the server returns a 500', (done) => {
    const errorResponse = new HttpErrorResponse({
      error: '500 Internal Server Error',
      status: 500, statusText: 'Internal Server Error'
    });

    httpClientSpy.get.and.returnValue(defer(() => Promise.reject(errorResponse)));

    templateService.getTemplates().subscribe(
      () => fail('expected an error'),
      error => {
        expect(error.message).toContain('500 Internal Server Error');
        done();
      }
    );
  });
});
