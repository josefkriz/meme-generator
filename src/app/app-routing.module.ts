import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemeEditorComponent } from './meme-editor/meme-editor.component';
import { MemeListComponent } from './meme-list/meme-list.component';

const routes: Routes = [
  { path: 'editor', component: MemeEditorComponent },
  { path: '', component: MemeListComponent},
  { path: '**', redirectTo: '' } // fallback to root
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
