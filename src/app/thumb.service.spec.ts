import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { defer } from 'rxjs/index';

import { ThumbService } from './thumb.service';
import { Meme } from './meme';

let httpClientSpy: { post: jasmine.Spy };
let thumbService: ThumbService;

const testMeme: Meme = {
  'created': 1525265961794,
  'thumbsDown': 1,
  'thumbsUp': 0,
  'id': '1525265961794.png'
};
const thumbKind = 'up';

describe('Thumb service: send thumb', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    thumbService = new ThumbService(<any> httpClientSpy);
  });

  it('should return nothing when successful', (done) => {
    httpClientSpy.post.and.returnValue(defer(() => Promise.resolve(null)));

    thumbService.giveThumb(testMeme, thumbKind).subscribe(
      res => {
        expect(res).toBe(null);
        expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
        done();
      },
      fail
    );
  });

  it('should return an error when the server returns a 500', (done) => {
    const errorResponse = new HttpErrorResponse({
      error: '500 Internal Server Error',
      status: 500, statusText: 'Internal Server Error'
    });

    httpClientSpy.post.and.returnValue(defer(() => Promise.reject(errorResponse)));

    thumbService.giveThumb(testMeme, thumbKind).subscribe(
      () => fail('expected an error'),
      error => {
        expect(error.message).toContain('500 Internal Server Error');
        done();
      }
    );
  });
});
