export interface Meme {
  id: string;
  created: number;
  thumbsUp: number;
  thumbsDown: number;
}
