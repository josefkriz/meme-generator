import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { defer } from 'rxjs/index';
import { Meme } from './meme';
import { MemeService } from './meme.service';

let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy };
let memeService: MemeService;

describe ('Meme service: get memes', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    memeService = new MemeService(<any> httpClientSpy);
  });

  it('should return memes', (done) => {
    const mockResponse = {
      'Items': [
        {'created': 1525265961794, 'thumbsDown': 1, 'thumbsUp': 0, 'id': '1525265961794.png'},
        {'created': 1525265362656, 'thumbsDown': 123, 'thumbsUp': 321, 'id': '1525265362656.png'}],
      'Count': 2,
      'ScannedCount': 2
    };

    const expectedMemes: Meme[] = [
      {'created': 1525265961794, 'thumbsDown': 1, 'thumbsUp': 0, 'id': '1525265961794.png'},
      {'created': 1525265362656, 'thumbsDown': 123, 'thumbsUp': 321, 'id': '1525265362656.png'}
    ];

    httpClientSpy.get.and.returnValue(defer(() => Promise.resolve(mockResponse)));

    memeService.getMemes().subscribe(
      memes => {
        expect(memes).toEqual(expectedMemes, 'expected memes'),
        expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
        done();
      }
    );
  });

  it('should return an error when the server returns a 500', (done) => {
    const errorResponse = new HttpErrorResponse({
      error: '500 Internal Server Error',
      status: 500, statusText: 'Internal Server Error'
    });

    httpClientSpy.get.and.returnValue(defer(() => Promise.reject(errorResponse)));

    memeService.getMemes().subscribe(
      () => fail('expected an error'),
      error => {
        expect(error.message).toContain('500 Internal Server Error');
        done();
      }
    );
  });
});

describe ('Meme service: save meme', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    memeService = new MemeService(<any> httpClientSpy);
  });

  it('should return a meme object when successful', (done) => {
    const mockImageData = 'fakeBase64code';
    const expectedMeme: Meme = {
      'created': 1525265961794,
      'thumbsDown': 1,
      'thumbsUp': 0,
      'id': '1525265961794.png'
    };

    httpClientSpy.post.and.returnValue(defer(() => Promise.resolve(expectedMeme)));

    memeService.saveMeme(mockImageData).subscribe(
      res => {
        expect(res).toEqual(expectedMeme);
        expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
        done();
      },
      fail
    );
  });

  it('should return an error when the server returns a 500', (done) => {
    const mockImageData = 'fakeBase64code';
    const errorResponse = new HttpErrorResponse({
      error: '500 Internal Server Error',
      status: 500, statusText: 'Internal Server Error'
    });

    httpClientSpy.post.and.returnValue(defer(() => Promise.reject(errorResponse)));

    memeService.saveMeme(mockImageData).subscribe(
      () => fail('expected an error'),
      error => {
        expect(error.message).toContain('500 Internal Server Error');
        done();
      }
    );
  });
});
