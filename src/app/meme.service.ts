import { throwError as observableThrowError, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {Meme} from './meme';

const API_URL = 'https://5yy6lo10z7.execute-api.us-east-1.amazonaws.com/Test';

@Injectable()
export class MemeService {

  constructor(private http: HttpClient) { }

  public saveMeme(imageData: string): Observable<Meme> {
    return this.http
      .post<Meme>(`${API_URL}/meme`, {image: imageData}).pipe(
        catchError((error: Response | any) => {
          console.error('API Service call failed: ', error);
          return observableThrowError(error);
        }));
  }

  public getMemes(): Observable<Meme[]> {
    return this.http
      .get(`${API_URL}/meme`)
      .pipe(
        map(response => response['Items']),
        catchError((error: Response | any) => {
          console.error('API Service call failed: ', error);
          return observableThrowError(error);
        }));
  }

}
