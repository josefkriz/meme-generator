import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MemeEditorComponent } from './meme-editor/meme-editor.component';
import { MemeTemplateGalleryComponent } from './meme-template-gallery/meme-template-gallery.component';
import { TemplateService } from './template.service';
import { MemeService } from './meme.service';
import { MemeListComponent } from './meme-list/meme-list.component';
import { AppRoutingModule } from './app-routing.module';
import { ThumbService } from './thumb.service';
import { ColorPickerModule } from 'ngx-color-picker';
import { ClipboardModule } from 'ngx-clipboard';
import { ShareModule } from '@ngx-share/core';


@NgModule({
  declarations: [
    AppComponent,
    MemeEditorComponent,
    MemeTemplateGalleryComponent,
    MemeListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ColorPickerModule,
    ClipboardModule,
    ShareModule.forRoot()
  ],
  providers: [TemplateService, MemeService, ThumbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
