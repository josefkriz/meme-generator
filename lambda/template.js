const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = (event) => {
  const request = (status, message) => ({
    statusCode: status,
    body: message,
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
  });

  switch (event.httpMethod) {
    case 'GET':
      return dynamo.scan({ TableName: 'Templates' }).promise()
        .then(
          (res) => request(200, JSON.stringify(res)),
          (err) => request(500, err.message)
        );
    default:
      return new Promise.reject(new Error(`Unsupported method '${event.httpMethod}'`));
  }
};
