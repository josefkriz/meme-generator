const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = (event) => {
  if (event.httpMethod !== 'POST') return new Promise.reject(new Error(`Unsupported method '${event.httpMethod}'`));

  const request = (status, message) => ({
    statusCode: status,
    body: message,
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
  });

  const requestBody = JSON.parse(event.body);

  if ( !('id' in requestBody) // check for existing properties
    || !('created' in requestBody)
    || !('kind' in requestBody)) return new Promise.reject(new Error('Invalid request body'));

  const memeID = requestBody.id;
  const memeCreated = requestBody.created;
  let kind;

  if (requestBody.kind === 'up') kind = "thumbsUp";
  else if (requestBody.kind === 'down') kind = "thumbsDown";
  else return new Promise.reject(new Error('Invalid request body'));

  const params = {
    TableName: 'Memes',
    Key: { id: memeID, created: memeCreated },
    UpdateExpression: `set ${kind} = ${kind} + :num`,
    ExpressionAttributeValues: { ':num': 1 },
    ReturnValues: 'ALL_NEW'
  };

  return dynamo.update(params).promise()
    .then(
      (res) => request(200, JSON.stringify(res)),
      (err) => request(500, err.message)
    );
};
