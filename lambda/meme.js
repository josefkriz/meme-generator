const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const dynamo = new AWS.DynamoDB.DocumentClient();

const MAX_IMAGE_SIZE = 3 * 1024 * 1024; // 3MB

exports.handler = (event) => {

  const request = (status, message) => ({
    statusCode: status,
    body: message,
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
  });

  const post = () => {
    const timestamp = Date.now();
    let encodedImage = JSON.parse(event.body).image;
    let decodedImage = Buffer.from(encodedImage, 'base64');

    if (decodedImage.length > MAX_IMAGE_SIZE) return new Promise.reject(new Error(`The uploaded image is too large. Maximum size is ${MAX_IMAGE_SIZE} bytes.`));

    const fileName = timestamp + ".png";
    const filePath = "memes/" + fileName;

    const meme = {
      id: fileName,
      created: timestamp,
      thumbsUp: 0,
      thumbsDown: 0
    };

    const s3params = {
      "Body": decodedImage,
      "Bucket": "aws-website-memegenerator-images",
      "Key": filePath
    };

    const dbparams = {
      TableName: "Memes",
      Item: meme
    };

    return Promise.all([
      s3.upload(s3params).promise(),
      dynamo.put(dbparams).promise()
    ]).then(
      ()    => request(200, JSON.stringify(meme)),
      (err) => request(500, err.message)
    );
  };

  switch (event.httpMethod) {
    case 'GET':
      return dynamo.scan({ TableName: "Memes" }).promise()
        .then(
          (res) => request(200, JSON.stringify(res)),
          (err) => request(500, err.message)
        );
    case 'POST':
      return post();
    default:
      return new Promise.reject(new Error(`Unsupported method '${event.httpMethod}'`));
  }
};
